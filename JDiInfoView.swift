//
//  JDiInfoView.swift
//  JD
//
//  Created by qualcentric on 3/3/21.
//  Copyright © 2021 qualcentric.net. All rights reserved.
//

import UIKit
import SwiftyJSON

enum DragDirection {
    case none
    case Up
    case Down
}

protocol JDiInfoViewDelegate {
    func onJDInfoView(didDragged sender: JDiInfoView, in direction: DragDirection)
    func onJDInfoView(goBack sender: JDiInfoView)
}

class JDiInfoView: UIView {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var draggableView: UIView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var pagerView: UIPageControl!
    // User Info
    @IBOutlet var headerView: UIView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var imgDragStatus: UIImageView!
    // Actions
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var btnPass: UIButton!
    // About
    @IBOutlet var aboutView: UIView!
    @IBOutlet var lblAbout: UILabel!
    
    fileprivate let panGestureRecognizer: UIPanGestureRecognizer = UIPanGestureRecognizer.init()
    
    fileprivate var yFromCenter: CGFloat = 0
    fileprivate var originalPoint: CGPoint = CGPoint(x: 0, y: 0)
    // Distance from center where the action applies. Higher = swipe further in order for the action to be called
    fileprivate var _ActionMargin: CGFloat = 0
    fileprivate var dragDirection: DragDirection = .none
    
    fileprivate var hasDistance: Bool = false
    fileprivate var hasAddress: Bool = false
    
    fileprivate var previousPageIndex: Int = 0
    
    var delegate: JDiInfoViewDelegate?
    
    var JDInfo: JDJDInfo? {
        didSet {
            self.updateJDInfo()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    // Configure the view...
    func initJDInfoView() {
        // Init pan gesture
        if self.draggableView != nil {
            self.draggableView.addGestureRecognizer(self.panGestureRecognizer)
            self.panGestureRecognizer.addTarget(self, action: #selector(JDiInfoView.onDragged(_:)))
            
            // self.collectionView.collectionViewLayout = EqulSizeLayout()
        }
    }
    
    // MARK: - IBAction methods
    @IBAction func onPass(_ sender: UIButton) {
        self.invokeGoDown(withDuration: 0.4)
    }
    
    @IBAction func onLike(_ sender: UIButton) {
        self.invokeGoUp()
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        if let del = self.delegate {
            del.onJDInfoView(goBack: self)
            self.removeGestureRecognizer(self.panGestureRecognizer)
        }
    }
    
    // MARK: - Gesture recognizer handling
    // Track pan gesture
    func onDragged(_ gestureRecognizer: UIPanGestureRecognizer) {
        self.yFromCenter = gestureRecognizer.translation(in: self.draggableView).y
        
        switch gestureRecognizer.state {
        case .began:
            // Just started swiping
            if self._ActionMargin == 0 {
                _ActionMargin = self.draggableView.frame.size.height - (self.draggableView.frame.size.height * 0.6)
            }
            self.originalPoint = self.draggableView.center
            break
        case .changed:
            // Move the object's center by center + gesture coordinate
            self.draggableView.center = CGPoint.init(x: self.originalPoint.x, y: self.originalPoint.y + yFromCenter)
            if self.dragDirection == .none {
                UIView.animate(withDuration: 0.2, animations: {
                    self.imgDragStatus.alpha = 0.4
                })
            }
            if yFromCenter > 0 {
                if self.dragDirection == .none || self.dragDirection == .Up {
                    self.dragDirection = .Down
                    self.changeDragStatus()
                }
            } else {
                if self.dragDirection == .none || self.dragDirection == .Down {
                    self.dragDirection = .Up
                    self.changeDragStatus()
                }
            }
            break
        case .ended:
            // Let go of the card
            self.onDragFinished()
            break
        default:
            break
        }
    }
    
    func changeDragStatus() {
        if self.dragDirection == .Up {
            self.imgDragStatus.image = UIImage.init(named: "like_JD.png")
        } else if self.dragDirection == .Down {
            self.imgDragStatus.image = UIImage.init(named: "pass_JD.png")
        }
    }
    
    func onChangeAlpha(_ distance: CGFloat) {
        self.draggableView.alpha = CGFloat(max(1 - (fabsf(Float.init(distance)) / Float(_ActionMargin)), 0.2))
    }
    
    func onDragFinished() {
        self.dragDirection = .none
        if yFromCenter < -_ActionMargin {
            self.invokeGoUp()
        } else if yFromCenter > _ActionMargin {
            self.invokeGoDown(withDuration: 0.3)
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                self.draggableView.center = self.originalPoint
                self.draggableView.alpha = 1
                self.imgDragStatus.alpha = 0
            })
        }
    }
    
    // MARK: - Other methods
    func updateJDInfo() {
        guard let JD = self.JDInfo else {
            return
        }
        
        if self.lblName != nil {
            if JD.age > 0 {
                self.lblName.text = "\(JD.firstName), \(JD.age)"
            } else {
                self.lblName.text = JD.firstName
            }
            var placeHolder: String = "male.jpg"
            if JD.gender == "2" {
                placeHolder = "female.jpg"
            }
            if let profileUrl = URL(string: JD.profilePic) {
                self.imgProfile.af_setImage(withURL: profileUrl, placeholderImage: UIImage(named: placeHolder)!)
            } else {
                self.imgProfile.image = UIImage(named: placeHolder)!
            }
            if JD.desc.length > 0 {
                self.lblAbout.text = JD.desc
            } else {
                self.lblAbout.text = "-"
            }
        }
        
//        self.pagerView.numberOfPages = JD.profilePics.count
//        self.collectionView.reloadData()
        
        self.performGetDistance(JD.facebookId)
        self.performFetchAddressFrom(latitude: JD.location.latitude, longitude: JD.location.longitude)
    }
    
    func invokeGoUp() {
        UIView.animate(withDuration: 0.3, animations: {
            self.draggableView.center = CGPoint.init(x: self.draggableView.center.x, y: self.originalPoint.y - self.draggableView.frame.size.height)
        }, completion: { (complete) in
            self.removeFromSuperview()
        })
        // Invoke delegate
        if let del = self.delegate {
            del.onJDInfoView(didDragged: self, in: .Up)
            self.draggableView.removeGestureRecognizer(self.panGestureRecognizer)
        }
    }
    
    func invokeGoDown(withDuration duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: {
            self.draggableView.center = CGPoint.init(x: self.draggableView.center.x, y: self.originalPoint.y + self.frame.size.height + 200)
        }, completion: { (complete) in
            self.removeFromSuperview()
        })
        // Invoke delegate
        if let del = self.delegate {
            del.onJDInfoView(didDragged: self, in: .Down)
            self.draggableView.removeGestureRecognizer(self.panGestureRecognizer)
        }
    }
    
    // MARK: - WSCall
    // 1. Get distance
    func performGetDistance(_ JDId: String) {
        var params: Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
        params["ent_Fuser_fbid"] = Globals.shared.facebookId as AnyObject?
        params["ent_Suser_fbid"] = JDId as AnyObject?
        
        WebRequest.POST(ApiUrls.findDistance, authType: .none, params: params, complition: { (result) -> Void in
            if let responseResult = result {
                self.processGetDistanceResult(responseResult)
            }
        }) { (error) -> Void in
            // Handle error
        }
    }
    
    func processGetDistanceResult(_ result: AnyObject) {
        let error: JDErrorInfo = JDErrorInfo.init(result)
        if error.hasError {
            // Handle error
        } else {
            let jsonResult = JSON(result)
            if let distance = jsonResult["userDistance"].string {
                if self.lblLocation != nil {
                    self.hasDistance = true
                    if self.hasAddress {
                        self.lblLocation.text = self.lblLocation.text! + " - \(distance)km away"
                    } else {
                        self.lblLocation.text = "\(distance)km away"
                    }
                }
            }
        }
    }
    
    // 2. Fetch address string from geo location coordinates
    func performFetchAddressFrom(latitude lati: String, longitude long: String) {
        if let queryEncoding = (lati + "," + long).addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            let queryUrl: String = "https://maps.google.com/maps/api/geocode/json?sensor=false&address=" + queryEncoding
            print("Query Url: \(queryUrl)")
            WebRequest.POST(queryUrl, authType: .none, params: nil, complition: { (result) -> Void in
                if let responseResult = result {
                    self.processFetchAddressResult(responseResult)
                }
            }) { (error) -> Void in
                // handle error here
            }
        }
    }
    
    func processFetchAddressResult(_ result: AnyObject) {
        let jsonResult = JSON(result)
        // Find City + State from address list
        if let resultAddresses = jsonResult["results"].array {
            var locationAddress: String = ""
            if resultAddresses.count > 0 {
                for address in resultAddresses {
                    if let addressComponents = address["address_components"].array {
                        if addressComponents.count > 0 {
                            for component in addressComponents {
                                if let types = component["types"].arrayObject {
                                    for type in types {
                                        if let val = type as? String, val == "administrative_area_level_2" {
                                            if let city = component["long_name"].string {
                                                if locationAddress.length > 0 {
                                                    locationAddress = city + ", " + locationAddress
                                                } else {
                                                    locationAddress = city
                                                }
                                            }
                                        } else if let val = type as? String, val == "administrative_area_level_1" {
                                            if let state = component["short_name"].string {
                                                if locationAddress.length > 0 {
                                                    locationAddress = locationAddress + ", " + state
                                                } else {
                                                    locationAddress = state
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break
                }
            }
            
            if locationAddress.length > 0 {
                self.hasAddress = true
                if self.hasDistance {
                    self.lblLocation.text = locationAddress + " - " + self.lblLocation.text!
                } else {
                    self.lblLocation.text = locationAddress
                }
            }
        }
    }
}

// MARK: - UICollectionViewDataSource & delegate
extension JDiInfoView: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let JD = self.JDInfo {
            return JD.profilePics.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as! ImageCollectionViewCell
        
        // Configure the cell
        if let JD = self.JDInfo {
            cell.imageUrl = URL.init(string: JD.profilePics[indexPath.row])
        }
        
        return cell
    }
    
    // Calculate current visible item count
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPageIndex: Int = Int(round(scrollView.contentOffset.x / scrollView.frame.size.width))
        
        if currentPageIndex != previousPageIndex {
            previousPageIndex = currentPageIndex
            self.pagerView.currentPage = currentPageIndex
        }
    }
}
